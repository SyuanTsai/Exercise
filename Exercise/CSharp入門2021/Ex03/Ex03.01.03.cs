﻿namespace CSharp入門2021.Ex03;

// ReSharper disable once InconsistentNaming
public class Ex03_01_03
{
    public Ex03_01_03()
    {
        Q1();
        Q2();
        Q3();
        Q4();
    }

    /// <summary>
    ///  Q1
    ///     先心算以下計算式, 再寫程式驗證結果與您想的是否相同
    ///     1 + (2+2) * 3 -1
    ///     2 + ((2 + 1) / 3 + 1 ) *2
    /// </summary>
    static void Q1()
    {
        DisplayHeader("Q1");
        /*  
         *  Q1.1    1 + (2+2) * 3 -1
         *  Step1.  1 + 4 * 3 - 1
         *  Step2.  1 + 12 - 1
         *  Step3.  12
         * */

        Console.WriteLine("Q1.1：1 + (2+2) * 3 -1");
        Console.WriteLine(1 + (2 + 2) * 3 - 1);
        /*
         *  Q1.2    2 + ((2 + 1) / 3 + 1 ) *2
         *  Step1.  2 + (3 / 3 + 1 ) * 2 
         *  Step2.  2 + ( 1 + 1 ) * 2
         *  Step3.  2 + 2 * 2
         *  Step4.  2 + 4
         *  Step5.  6 
         */
        Console.WriteLine("Q1.2：2 + ((2 + 1) / 3 + 1 ) *2");
        Console.WriteLine(2 + ((2 + 1) / 3 + 1) * 2);
    }

    /// <summary>
    ///  Q2.
    ///     寫程式, 計算並顯示 120 除以 7 的商數, 餘數, 各是多少
    /// </summary>
    static void Q2()
    {
        DisplayHeader("Q2");
        const int quotient = 120 / 7;
        Console.WriteLine($"120 除以 7 的商數等於{quotient}");

        const int remainder = 120 % 7;
        Console.WriteLine($"120 除以 7 的餘數等於{remainder}");
    }

    /// <summary>
    ///  Q3.
    ///     寫程式, 計算並顯示 1 + 2 + 3 + .... + 10 的總和
    /// </summary>
    static void Q3()
    {
        DisplayHeader("Q3");
        const int n = 10;
        const int sum = (1 + n) * n / 2;
        Console.WriteLine(sum);
    }

    /// <summary>
    ///  Q4
    ///     計算 9 與 202 分別是奇數或是偶數
    /// </summary>
    static void Q4()
    {
        DisplayHeader("Q4");
        const int numberOne = 9 % 2;
        var result = numberOne == 1 ? "奇數" : "偶數";
        Console.WriteLine($"9 為{result}");
        
        const int numberTwo = 202 % 2;
        result = numberTwo == 1 ? "奇數" : "偶數";
        Console.WriteLine($"202 為{result}");
    }

    static void DisplayHeader(string title)
    {
        Console.WriteLine("\r\n");
        Console.WriteLine(title);
        Console.WriteLine(new string('=', 40));
    }
}