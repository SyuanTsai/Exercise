﻿namespace CSharp入門2021.Ex03
{
// ReSharper disable once InconsistentNaming
    public class Ex03_02_04
    {
        public Ex03_02_04()
        {
            Q1();
            Q2();
            Q3();
            Q4();
        }
        /// <summary>
        ///     Q1.
        ///         宣告字串變數, 包含以下值, 並用 Console.WriteLine() 將它忠實地呈現在畫面上
        ///         D:\temp\animal.jpg
        /// </summary>
        static void Q1()
        {
            DisplayHeader("Q1");
            Console.WriteLine(@"D:\temp\animal.jpg");
        }
        
        /// <summary>
        ///     Q2.
        ///         宣告字串變數, 包含以下值, 並用 Console.WriteLine() 將它忠實地呈現在畫面上
        ///         我是一個"好學生", 會乖乖練習寫作業
        /// </summary>
        static void Q2()
        {
            DisplayHeader("Q2");
            Console.WriteLine("我是一個\"好學生\", 會乖乖練習寫作業");
        }
        
        /// <summary>
        ///     Q3.
        ///         宣告字串變數時不使用 @, 包含以下值以及折行, 並用 Console.WriteLine() 將它忠實地呈現在畫面上(呈現時也必需折行)
        ///         我是第一行文字
        ///         我是第二行文字
        /// </summary>
        static void Q3()
        {
            DisplayHeader("Q3");
            Console.WriteLine("我是第一行文字\n我是第二行文字");
        }
        
        /// <summary>
        ///     Q4
        ///         宣告字串變數時使用 @, 包含以下值以及折行, 並用 Console.WriteLine() 將它忠實地呈現在畫面上(呈現時也必需折行)
        ///         <select>
        ///             <option value="1">台北</option>
        ///             <option value="2">台中</option>
        ///         </select>
        /// </summary>
        static void Q4()
        {
            DisplayHeader("Q4");
            const string str =
@"<select>
   <option value=""1"">台北</option>
   <option value=""2"">台中</option>
</select>";
            
            Console.WriteLine(str);
        }

        static void DisplayHeader(string title)
        {
            Console.WriteLine("\r\n");
            Console.WriteLine(title);
            Console.WriteLine(new string('=', 40));
        }
    }
}