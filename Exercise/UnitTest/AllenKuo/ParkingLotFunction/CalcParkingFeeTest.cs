﻿using System;
using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.UnitFunction;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLotFunction;

public class CalcParkingFeeTest
{
    
    [SetUp]
    public void Setup()
    {
    }
    
    [Test]
    [TestCase("2002/5/1 00:10:00","2002/5/1 00:19:59", 0)]
    public void 同日但是不足十分鐘(DateTime start, DateTime end, int result)
    {
        var arr =  MultiDaysCalculation.CalcParkingFee(start, end);
        arr.TotalFee.Should().Be(result);
    }
    
    [Test]
    [TestCase("2002/5/1 23:49:00","2002/5/2 00:10:59", 0)]
    public void 跨日但是單日不足十分鐘(DateTime start, DateTime end, int result)
    {
        var arr =  MultiDaysCalculation.CalcParkingFee(start, end);
        arr.TotalFee.Should().Be(result);
    }
    
    [Test]
    [TestCase("2002/5/1 23:55:00","2002/5/2 00:11:59", 7)]
    public void 跨日但是只有一天日超過十分鐘(DateTime start, DateTime end, int result)
    {
        var arr =  MultiDaysCalculation.CalcParkingFee(start, end);
        arr.TotalFee.Should().Be(result);
    } 
    
    [Test]
    [TestCase("2002/5/1 23:48:00","2002/5/2 00:11:59", 14)]
    public void 跨日但是兩天都超過十分鐘(DateTime start, DateTime end, int result)
    {
        var arr =  MultiDaysCalculation.CalcParkingFee(start, end);
        arr.TotalFee.Should().Be(result);

    }
    
    [Test]
    [TestCase("2002/5/1 23:48:00","2002/5/3 00:11:59", 64)]
    public void 三日以上(DateTime start, DateTime end, int result)
    {
        var arr =  MultiDaysCalculation.CalcParkingFee(start, end);
        arr.TotalFee.Should().Be(result);
    }
}