﻿using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.UnitFunction;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLotFunction;

public class CostCalculationTest
{
    
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    [TestCase(0,0)]
    [TestCase(3,0)]
    [TestCase(5,0)]
    [TestCase(7,0)]
    [TestCase(9,0)]
    [TestCase(10,0)]
    public void 零到十分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(0);
    }
    
    [Test]
    [TestCase(11,7)]
    [TestCase(20,7)]
    [TestCase(15,7)]
    [TestCase(30,7)]
    public void 十一分鐘到三十分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(7);
    }

    [Test]
    [TestCase(31,10)]
    [TestCase(45,10)]
    [TestCase(50,10)]
    [TestCase(59,10)]
    public void 三十一分鐘到五十九分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(10);
    }
    [Test]
    [TestCase(61,17)]
    [TestCase(75,17)]
    [TestCase(80,17)]
    [TestCase(90,17)]
    public void 六十一分鐘到九十分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(17);
    }
    [Test]
    [TestCase(91,20)]
    [TestCase(100,20)]
    [TestCase(110,20)]
    [TestCase(119,20)]
    [TestCase(120,20)]
    public void 九十一分鐘到一一九分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(20);
    }
    
    [Test]
    [TestCase(121,27)]
    [TestCase(130,27)]
    [TestCase(140,27)]
    [TestCase(150,27)]
    public void 一二一分鐘到一五零分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(27);
    }
    
    [Test]
    [TestCase(151,30)]
    [TestCase(180,30)]
    public void 一五一分鐘到一八零分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(30);
    }
    
    [Test]
    [TestCase(181,37)]
    [TestCase(210,37)]
    public void 一八一分鐘到二一零分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(37);
    }
    
    [Test]
    [TestCase(211,40)]
    [TestCase(240,40)]
    public void 二一一分鐘到二四零分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(40);
    }
    [Test]
    [TestCase(241,47)]
    [TestCase(270,47)]
    public void 二四一分鐘到二七零分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(47);
    }
    [Test]
    [TestCase(271,50)]
    [TestCase(299,50)]
    [TestCase(300,50)]
    [TestCase(301,50)]
    [TestCase(1439,50)]
    public void 二七一分鐘到二九九分鐘的收費金額(int min, int money)
    {
        var assert = CostCalculation.TotalCost(min);

        assert.Should().Be(50);
    }
}