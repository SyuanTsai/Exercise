﻿using System;
using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.UnitFunction;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLotFunction;

public class TimeDiffForMinTest
{
    private DateTime _baseTime;

    [SetUp]
    public void Setup()
    {
        _baseTime = DateTime.Now;
    }

    [Test]
    public void 一日內分鐘差異()
    {
        var sTime = _baseTime.Date.AddHours(13).AddMinutes(10).AddSeconds(59);
        var eTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(10);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(1);
    }

    [Test]
    public void 一日內同分秒差()
    {
        var sTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(59);
        var eTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(10);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(0);
    }

    [Test]
    public void 一日內同分秒差Allen()
    {
        var sTime = _baseTime.Date.AddHours(9);
        var eTime = _baseTime.Date.AddHours(9).AddSeconds(59);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(0);
    }

    [Test]
    public void 一日以上同分鐘()
    {
        var sTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(59);
        var eTime = _baseTime.Date.AddDays(1).AddHours(13).AddMinutes(11).AddSeconds(59);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(1440);
    }

    [Test]
    public void 一日以上不同分鐘()
    {
        var sTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(59);
        var eTime = _baseTime.Date.AddDays(1).AddHours(13).AddMinutes(12).AddSeconds(45);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(1441);
    }

    [Test]
    public void 兩日以上同分鐘()
    {
        var sTime = _baseTime.Date.AddHours(13).AddMinutes(11).AddSeconds(59);
        var eTime = _baseTime.Date.AddDays(2).AddHours(13).AddMinutes(11).AddSeconds(59);
        var result = TimeDiff.TimeDiffMin(sTime, eTime);
        result.Should().Be(2880);
    }
}