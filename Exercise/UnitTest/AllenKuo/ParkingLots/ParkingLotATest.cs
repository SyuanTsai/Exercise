﻿using System;
using System.Linq;
using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.ParkingLots;
using logicLibrary.AllenKuo.ParkingLot.UnitFunction;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLots;

public class ParkingLotATest
{
    
    [SetUp]
    public void Setup()
    {
    }
    
    [Test]
    [TestCase("2022/5/2 09:00:00", "2022/5/2 09:10:59", 0, 1)] // 同一天
    [TestCase("2022/5/2 09:00:00", "2022/5/2 09:11:59", 7, 1)] // 同一天
    [TestCase("2022/5/2 09:00:00", "2022/5/2 10:00:59", 10, 1)] // 同一天
    [TestCase("2022/5/2 23:49:00", "2022/5/3 00:10:59", 0, 2)] // 跨一天,免費
    [TestCase("2022/5/2 23:48:00", "2022/5/3 00:00:00", 7, 2)] // 跨一天,收費
    [TestCase("2022/5/2 23:48:00", "2022/5/3 00:11:59", 14, 2)] // 跨一天,收費
    [TestCase("2022/5/2 00:00:00", "2022/5/3 00:11:59", 57, 2)] // 跨一天,收費
    [TestCase("2022/5/2 23:49:00", "2022/5/4 00:11:59", 57, 3)] // 跨2天,收費
    [TestCase("2022/5/2 22:59:00", "2022/5/4 00:11:59", 67, 3)] // 跨2天,收費
    [TestCase("2022/5/2 00:00:00", "2022/5/4 00:11:59", 107, 3)] // 跨2天,收費
    public void 功能測試(DateTime start, DateTime end, int result, int days)
    {
        var parking = new ParkingLotA();

        var arr = parking.CalcParkingFee(start, end);
        
        arr.TotalFee.Should().Be(result);
        arr.Items.Count().Should().Be(days);
    }
}