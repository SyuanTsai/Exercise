﻿using System;
using System.Linq;
using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.ParkingLots;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLots;

public class ParkingLotBTest
{
    
    [SetUp]
    public void Setup()
    {
    }
    
    [Test]
    [TestCase("2022/5/7 09:00:00", "2022/5/7 09:01:59", 15, 1)] // 1分鐘,15元
    [TestCase("2022/5/7 09:00:00", "2022/5/7 10:00:59", 15, 1)] // 60分鐘,15元
    [TestCase("2022/5/7 09:00:00", "2022/5/7 09:00:59", 0, 1)] // 0分, 0元
    [TestCase("2022/5/7 09:00:00", "2022/5/7 10:01:59", 30, 1)] // 61分, 30元
    [TestCase("2022/5/7 23:58:00", "2022/5/8 00:01:00", 30, 2)] // 跨一天,收費
    [TestCase("2022/5/7 00:00:00", "2022/5/8 00:11:59", 265, 2)] // 250 + 15

    public void 功能測試(DateTime start, DateTime end, int result, int days)
    {
        var parking = new ParkingLotB();

        var arr = parking.CalcParkingFee(start, end);
        
        arr.TotalFee.Should().Be(result);
        arr.Items.Count().Should().Be(days);
    }
}