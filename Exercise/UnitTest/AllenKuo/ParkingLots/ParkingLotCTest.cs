﻿using System;
using System.Linq;
using FluentAssertions;
using logicLibrary.AllenKuo.ParkingLot.ParkingLots;
using NUnit.Framework;

namespace UnitTest.AllenKuo.ParkingLots;

public class ParkingLotCTest
{
    
    [SetUp]
    public void Setup()
    {
    }
    
    [Test]
    [TestCase("2022/5/2 09:00:00", "2022/5/2 10:00:59", 20, 1)] // 同一天, 第一小時為20元
    [TestCase("2022/5/2 09:00:00", "2022/5/2 10:01:00", 50, 1)] // 同一天, 61分,第一小時20,第二小時起,每小時30
    [TestCase("2022/5/2 09:00:00", "2022/5/2 11:01:00", 80, 1)] // 2小又1分,第一小時20,第二小時起,每小時30
    [TestCase("2022/5/2 09:00:00", "2022/5/2 12:01:00", 110, 1)] // 3小又1分,第一小時20,第二小時起,每小時30
    [TestCase("2022/5/2 23:48:00", "2022/5/3 00:00:00", 20, 2)] // 跨一天,20 + 0
    [TestCase("2022/5/2 23:48:00", "2022/5/3 00:11:59", 40, 2)] // 跨一天,20 + 20
    [TestCase("2022/5/2 00:00:00", "2022/5/3 01:01:00", 350, 2)] // 跨一天,第二天1小時又1分, 300 + 50
    [TestCase("2022/5/2 23:49:00", "2022/5/4 00:11:59", 340, 3)] // 跨2天,20 + 300 + 20

    public void 平日功能測試(DateTime start, DateTime end, int result, int days)
    {
        var parking = new ParkingLotC();

        var arr = parking.CalcParkingFee(start, end);
        
        arr.TotalFee.Should().Be(result);
        arr.Items.Count().Should().Be(days);
    }
    
    [TestCase("2022/5/7 00:00:00", "2022/5/7 23:59:59", 0, 1)] // 1分鐘,假日免費
    [TestCase("2022/5/7 00:00:00", "2022/5/8 23:59:59", 0, 2)] // 跨一天,假日免費
    public void 假日功能測試(DateTime start, DateTime end, int result, int days)
    {
        var parking = new ParkingLotC();

        var arr = parking.CalcParkingFee(start, end);
        
        arr.TotalFee.Should().Be(result);
        arr.Items.Count().Should().Be(days);
    }
}