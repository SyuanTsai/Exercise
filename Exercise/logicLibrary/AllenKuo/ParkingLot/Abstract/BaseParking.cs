﻿using logicLibrary.AllenKuo.ParkingLot.Interface;
using logicLibrary.AllenKuo.ParkingLot.Models;

namespace logicLibrary.AllenKuo.ParkingLot.Abstract;

/// <summary>
///     停車場的基本設定
/// </summary>
public abstract class BaseParking : ITotalCost
{
    /// <summary>
    ///     金額的結算
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public abstract int TotalCost(int totalMin);
    
    /// <summary>
    ///     統計總收費金額
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public ParkingFee CalcParkingFee(DateTime start, DateTime end)
    {
        start = DateTime(start, ref end);

        var days = CalcFeeForMultiDays(start, end).ToList();

        foreach (var day in days)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        var result = new ParkingFee()
        {
            Items = days,
        };

        return result;
    }

    /// <summary>
    ///     傳回每天應付停車費的邏輯
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private IEnumerable<SingleDayFee> CalcFeeForMultiDays(DateTime start, DateTime end)
    {
        var result = new List<SingleDayFee>();

        //  起始日等於結束日
        if (start.Date.Equals(end.Date))
        {
            return SingleDay(start, end, result);
        }

        //  取出天數
        var dateRange = MultiDay(start, end);

        return dateRange;
    }

    /// <summary>
    ///     計算單日金額
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    protected virtual int CalcFee(DateTime start, DateTime end)
    {
        var totalMin = TimeDiffMin(start, end);
        var cost = TotalCost(totalMin);
        return cost;
    }

    /// <summary>
    ///     計算每日總分數
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    protected static int TimeDiffMin(DateTime start, DateTime end)
    {
        //  低於一天，只需要處理分鐘
        //  如果要開放外部使用的話需要補上這一段，因為不能防止別人輸入錯誤參數。
        //  同時下方的Math.Floor 要改 Math.Ceiling
        // if (end.Date == start.Date)
        // {
        //     var sMin = (int) start.TimeOfDay.TotalMinutes;
        //     var eMin = (int) end.TimeOfDay.TotalMinutes;
        //     var inDay = sMin - eMin;
        //
        //     return Math.Abs(inDay);
        // }

        //  算出差異
        var diff = (start - end).TotalMinutes;
        //  絕對值
        var abs = Math.Abs(diff);
        //  無條件進位
        var ceiling = (int) Math.Floor(abs);

        return ceiling;
    }

    #region 天數處理

    /// <summary>
    ///     起始日等於結束日直接進入給予單日資料節省效能。
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private static IEnumerable<SingleDayFee> SingleDay(DateTime start, DateTime end, ICollection<SingleDayFee> result)
    {
        var onlyDay = new SingleDayFee()
        {
            StartTime = start, EndTime = end
        };

        result.Add(onlyDay);

        return result;
    }


    /// <summary>
    ///     產生多日的資料
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    protected virtual IEnumerable<SingleDayFee> MultiDay(DateTime start, DateTime end)
    {
        //  建立天數長度(N+1日)
        var days = Enumerable.Range(0, 1 + (end.Date - start.Date).Days);

        //  生資料
        var enumerable = days.ToList();
        var dateRange = enumerable.Select(
            offset =>
            {
                //  定義每日23:59
                var dayEnd = start.Date.AddDays(offset + 1).AddMinutes(-1);

                if (offset == 0)
                {
                    //  起始時間  & 當日23:59
                    return new SingleDayFee(start, dayEnd);
                }

                return (offset == enumerable.Count - 1)
                    //  最後一天
                    ? new SingleDayFee(end.Date, end)
                    //  中間時段
                    : new SingleDayFee(start.Date.AddDays(offset), dayEnd);
            }).ToList();

        //  算金額
        foreach (var day in dateRange)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        return dateRange;
    }

    #endregion

    /// <summary>
    ///     協助日期交換的Function;
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static DateTime DateTime(DateTime start, ref DateTime end)
    {
        if (start > end)
        {
            (end, start) = (start, end);
        }

        return start;
    }

}