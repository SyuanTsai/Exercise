﻿namespace logicLibrary.AllenKuo.ParkingLot.Interface;

public interface ITotalCost
{
    public int TotalCost(int totalMin);
}