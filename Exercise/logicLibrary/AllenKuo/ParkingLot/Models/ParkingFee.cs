﻿namespace logicLibrary.AllenKuo.ParkingLot.Models;

public class ParkingFee
{
    public IEnumerable<SingleDayFee> Items { get; set; }

    public int TotalFee
    {
        get
        {
            var result = Items.Sum(d => d.Fee);
            return result;
        }
    }
}