﻿namespace logicLibrary.AllenKuo.ParkingLot.Models;

/// <summary>
///     單日計費
/// </summary>
public class SingleDayFee
{
    public SingleDayFee()
    {
    }

    public SingleDayFee(DateTime startTime, DateTime endTime)
    {
        this.StartTime = startTime;
        this.EndTime = endTime;
    }

    /// <summary>
    ///     精確到分鐘的入場時間
    /// </summary>
    public DateTime StartTime { get; set; }

    /// <summary>
    ///     精確到分鐘的離場時間
    /// </summary>
    public DateTime EndTime { get; set; }

    /// <summary>
    ///     本日應收取費用
    /// </summary>
    public int Fee { get; set; }
}