﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using logicLibrary.AllenKuo.ParkingLot.Abstract;
using logicLibrary.AllenKuo.ParkingLot.Interface;

namespace logicLibrary.AllenKuo.ParkingLot.ParkingLots;

/// <summary>
///     收費原則 B:
///     平日採用目前的數費費率;
///     假日(星期六,日,不含國定假日)每小時 15 元,
///     沒有免費時段,固定一小時15元, 不足一小時視為1小時, 每天最多收 250元
/// </summary>
public class ParkingLotB : BaseParking, ITotalCost
{
    private const int HourInterval = 60; //  小時顆粒度
    private const int HourCost = 10; //  小時收費
    private const int HolidayHourCost = 15; //  假日小時收費
    private const int HolidayMaxCost = 250; //  假日上限
    protected override int CalcFee(DateTime start, DateTime end)
    {
        //  計算分鐘數
        var totalMin = TimeDiffMin(start, end);
        //  切割假日
        switch (start.DayOfWeek)
        {
            case DayOfWeek.Saturday:
            case DayOfWeek.Sunday:
                var holidays = HolidaysTotalCost(totalMin);
                return holidays;
            default:
                var cost = TotalCost(totalMin);
                return cost;
        }

    }

    private static int HolidaysTotalCost(int totalMin)
    {
        var hours =  (int)Math.Ceiling((double)totalMin / HourInterval);

        var cost = hours * HolidayHourCost;

        return cost >= HolidayMaxCost ? HolidayMaxCost : cost;
    }


    /// <summary>
    ///     金額的結算
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public override int TotalCost(int totalMin)
    {
        //  不需要運算的資料直接拋回
        switch (totalMin)
        {
            case <= 10:
                return 0;
            case >= 300:
                return 50;
        }

        var result = totalMin / HourInterval * HourCost; //  取小時金額
        var min = totalMin % HourInterval; //  取分鐘

        result += MinCalculation(min); //  分鐘的計算邏輯拆出去

        return result;
    }

    /// <summary>
    ///     分鐘計算
    /// </summary>
    /// <param name="min"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static int MinCalculation([Range(0, 60)] int min)
    {
        const int lessHalf = 7; //  30分鐘內收費
        const int overHalf = 10; //  30分鐘外收費

        return min switch
        {
            0 => 0,
            > 0 and <= 30 => lessHalf,
            > 30 and < 60 => overHalf,
            _ => 0
        };
    }
}