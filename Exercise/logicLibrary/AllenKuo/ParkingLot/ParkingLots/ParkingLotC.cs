﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using logicLibrary.AllenKuo.ParkingLot.Abstract;
using logicLibrary.AllenKuo.ParkingLot.Interface;
using logicLibrary.AllenKuo.ParkingLot.Models;

namespace logicLibrary.AllenKuo.ParkingLot.ParkingLots;

/// <summary>
///     收費原則 C:
///     六日收費: 免費
///     平日收費 :累進費率,第一小時為20元、第二小時起每小時30元, 平日每天最多收300元
/// </summary>
public class ParkingLotC : BaseParking, ITotalCost
{
    private const int HourInterval = 60; //  小時顆粒度
    private const int OnlyHourCost = 20; //  小時收費
    private const int HolidayMaxCost = 0; //  假日上限
    private const int DayMaxCost = 300; //  平日上限
    protected override int CalcFee(DateTime start, DateTime end)
    {
        //  計算分鐘數
        var totalMin = TimeDiffMin(start, end);
        //  切割假日
        switch (start.DayOfWeek)
        {
            case DayOfWeek.Saturday:
            case DayOfWeek.Sunday:
                return HolidayMaxCost;
            default:
                var cost = TotalCost(totalMin);
                return cost;
        }

    }

    /// <summary>
    ///     金額的結算
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public override int TotalCost(int totalMin)
    {
        //  不需要運算的資料直接拋回
        switch (totalMin)
        {
            //  免費額度
            case <= 10:
                return 0;
            //  只停一小時以內，基本額度
            case <= HourInterval:
                return OnlyHourCost;
            //  10H內為290元 
            case > 300:
                return DayMaxCost;
        }
        
        //  只要處理61~299以內的價格就好
        
        //  取得總小時數
        var hours =  (int)Math.Ceiling((double)totalMin / HourInterval);
        var result =  (hours - 1) * 30 + OnlyHourCost;
        
        return result;
    }
    
    /// <summary>
    ///     不知道為何這邊的
    ///     [TestCase("2022/5/2 23:49:00", "2022/5/4 00:11:59", 340, 3)] // 跨2天,20 + 300 + 20
    ///     第一天要收錢，這樣代表美日結算時間從59分變成隔日的00:00
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    protected override IEnumerable<SingleDayFee> MultiDay(DateTime start, DateTime end)
    {
        //  建立天數長度(N+1日)
        var days = Enumerable.Range(0, 1 + (end.Date - start.Date).Days);

        //  生資料
        var enumerable = days.ToList();
        var dateRange = enumerable.Select(
            offset =>
            {
                //  定義隔日00:00
                var dayEnd = start.Date.AddDays(offset + 1);

                if (offset == 0)
                {
                    //  起始時間  & 當日23:59
                    return new SingleDayFee(start, dayEnd);
                }

                return (offset == enumerable.Count - 1)
                    //  最後一天
                    ? new SingleDayFee(end.Date, end)
                    //  中間時段
                    : new SingleDayFee(start.Date.AddDays(offset), dayEnd);
            }).ToList();

        //  算金額
        foreach (var day in dateRange)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        return dateRange;
    }

}