﻿using System.ComponentModel.DataAnnotations;

public class Solution
{
    public static ParkingFee CalcParkingFee(DateTime start, DateTime end)
    {
        start = DateTime(start, ref end);

        var days = CalcFeeForMultiDays(start, end).ToList();

        foreach (var day in days)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        var result = new ParkingFee()
        {
            Items = days,
        };

        return result;
    }

    /// <summary>
    ///     傳回每天應付停車費的邏輯
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static IEnumerable<SingleDayFee> CalcFeeForMultiDays(DateTime start, DateTime end)
    {
        var result = new List<SingleDayFee>();

        //  因為是public 所以還是要防呆
        start = DateTime(start, ref end);

        //  當日 - 不需要做額外處理
        if (start.Date.Equals(end.Date))
        {
            return SingleDay(start, end, result);
        }

        //  取出天數
        var dateRange = MultiDay(start, end);

        return dateRange;
    }

    /// <summary>
    ///     產生一日的資料
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private static IEnumerable<SingleDayFee> SingleDay(DateTime start, DateTime end, List<SingleDayFee> result)
    {
        var onlyDay = new SingleDayFee()
        {
            StartTime = start, EndTime = end, Fee = CalcFee(start, end)
        };

        result.Add(onlyDay);

        return result;
    }

    /// <summary>
    ///     產生多日的資料
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static IEnumerable<SingleDayFee> MultiDay(DateTime start, DateTime end)
    {
        //  因為是public 所以還是要防呆
        start = DateTime(start, ref end);

        //  建立天數長度(N+1日)
        var days = Enumerable.Range(0, 1 + (end.Date-start.Date).Days );

        //  生資料
        var enumerable = days.ToList();
        var dateRange = enumerable.Select(
            offset =>
            {
                //  定義每日23:59
                var dayEnd = start.Date.AddDays(offset + 1).AddMinutes(-1);

                if (offset == 0)
                {
                    //  起始時間  & 當日23:59
                    return new SingleDayFee(start, dayEnd);
                }

                return (offset == enumerable.Count - 1)
                    //  最後一天
                    ? new SingleDayFee(end.Date, end)
                    //  中間時段
                    : new SingleDayFee(start.Date.AddDays(offset), dayEnd);
            }).ToList();

        //  算金額
        foreach (var day in dateRange)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        return dateRange;
    }


    /// <summary>
    ///     計算單日金額
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static int CalcFee(DateTime start, DateTime end)
    {
        var totalMin = TimeDiffMin(start, end);
        var cost = TotalCost(totalMin);
        return cost;
    }

    /// <summary>
    ///     分鐘數計算
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static int TimeDiffMin(DateTime start, DateTime end)
    {
        //  低於一天，只需要處理分鐘
        if ((end.Date - start.Date).Days < 1)
        {
            var sMin = (int) start.TimeOfDay.TotalMinutes;
            var eMin = (int) end.TimeOfDay.TotalMinutes;
            var inDay = sMin - eMin;

            return Math.Abs(inDay);
        }

        //  算出差異
        var diff = (start - end).TotalMinutes;
        //  絕對值
        var abs = Math.Abs(diff);
        //  無條件進位
        var ceiling = (int) Math.Ceiling(abs);

        return ceiling;
    }

    /// <summary>
    ///     單日總額
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public static int TotalCost(int totalMin)
    {
        //  不需要運算的資料直接拋回
        switch (totalMin)
        {
            case <= 10:
                return 0;
            case >= 300:
                return 50;
        }

        const int hourInterval = 60; //  小時顆粒度
        const int hourCost = 10; //  小時收費

        var result = totalMin / hourInterval * hourCost; //  取小時金額
        var min = totalMin % hourInterval; //  取分鐘

        result += MinCalculation(min); //  分鐘的計算邏輯拆出去

        return result;
    }

    /// <summary>
    ///     分鐘計算
    /// </summary>
    /// <param name="min"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static int MinCalculation([Range(0, 60)] int min)
    {
        const int lessHalf = 7; //  30分鐘內收費
        const int overHalf = 10; //  30分鐘外收費

        return min switch
        {
            0 => 0,
            > 0 and <= 30 => lessHalf,
            > 30 and < 60 => overHalf,
            _ => 0
        };
    }

    /// <summary>
    ///     協助日期交換的Function;
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static DateTime DateTime(DateTime start, ref DateTime end)
    {
        if (start > end)
        {
            (end, start) = (start, end);
        }

        return start;
    }


    /// <summary>
    ///     總結算
    /// </summary>
    public class ParkingFee
    {
        public IEnumerable<SingleDayFee> Items { get; set; }

        public int TotalFee
        {
            get
            {
                var result = Items.Sum(d => d.Fee);
                return result;
            }
        }
    }


    /// <summary>
    ///     單日計費
    /// </summary>
    public class SingleDayFee
    {
        public SingleDayFee()
        {
        }

        public SingleDayFee(DateTime startTime, DateTime endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
        }

        /// <summary>
        ///     精確到分鐘的入場時間
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        ///     精確到分鐘的離場時間
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        ///     本日應收取費用
        /// </summary>
        public int Fee { get; set; }
    }
}