﻿namespace logicLibrary.AllenKuo.ParkingLot.UnitFunction;

/// <summary>
///     開啟來補測案的
/// </summary>
public static class CalcFeeHelper
{
    // 傳入的是時間起迄, 並非分鐘數
    public static int CalcFee(DateTime start, DateTime end)
    {
        var totalMin = TimeDiff.TimeDiffMin(start, end);
        var cost = CostCalculation.TotalCost(totalMin);
        return cost;
    }

}