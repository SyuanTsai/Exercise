﻿using System.ComponentModel.DataAnnotations;

namespace logicLibrary.AllenKuo.ParkingLot.UnitFunction;

public static class CostCalculation
{
    /// <summary>
    ///     故意依照案例來撰寫程式與測試，
    ///     TDD模式的驗證。
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public static int TotalCost(int totalMin)
    {
        //  不需要運算的資料直接拋回
        switch (totalMin)
        {
            case <= 10:
                return 0;
            case >= 300:
                return 50;
        }

        const int hourInterval = 60; //  小時顆粒度
        const int hourCost = 10; //  小時收費

        var result = totalMin / hourInterval * hourCost; //  取小時金額
        var min = totalMin % hourInterval; //  取分鐘
        
        result += MinCalculation(min);//  分鐘的計算邏輯拆出去

        return result;
    }

    /// <summary>
    ///     分鐘計算
    /// </summary>
    /// <param name="min"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private static int MinCalculation([Range(0, 60)] int min)
    {
        const int lessHalf = 7; //  30分鐘內收費
        const int overHalf = 10; //  30分鐘外收費

        return min switch
        {
            0 => 0,
            > 0 and <= 30 => lessHalf,
            > 30 and < 60 => overHalf,
            _ => 0
        };
    }


    /// <summary>
    ///     故意依照案例來撰寫程式與測試，
    ///     TDD模式的驗證。
    /// </summary>
    /// <param name="totalMin"></param>
    /// <returns></returns>
    public static int TotalCostVerOne(int totalMin)
    {
        return totalMin switch
        {
            <= 10 => 0,
            <= 30 => 7,
            <= 60 => 10,
            <= 90 => 17,
            <= 120 => 20,
            <= 150 => 27,
            <= 180 => 30,
            <= 210 => 37,
            <= 240 => 40,
            <= 270 => 47,
            <= 300 => 50,
            > 300 => 50
        };
    }
}