﻿using logicLibrary.AllenKuo.ParkingLot.Models;

namespace logicLibrary.AllenKuo.ParkingLot.UnitFunction;

public static class MultiDaysCalculation
{
    public static ParkingFee CalcParkingFee(DateTime start, DateTime end)
    {
        start = DateTime(start, ref end);

        var days = CalcFeeForMultiDays(start, end).ToList();

        foreach (var day in days)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        var result = new ParkingFee()
        {
            Items = days,
        };

        return result;
    }

    /// <summary>
    ///     協助日期交換的Function;
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static DateTime DateTime(DateTime start, ref DateTime end)
    {
        if (start > end)
        {
            (end, start) = (start, end);
        }

        return start;
    }

    /// <summary>
    ///     傳回每天應付停車費的邏輯
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static IEnumerable<SingleDayFee> CalcFeeForMultiDays(DateTime start, DateTime end)
    {
        var result = new List<SingleDayFee>();

        //  因為是public 所以還是要防呆
        start = DateTime(start, ref end);

        //  當日 - 不需要做額外處理
        if (start.Date.Equals(end.Date))
        {
            return SingleDay(start, end, result);
        }

        //  取出天數
        var dateRange = MultiDay(start, end);

        return dateRange;
    }

    /// <summary>
    ///     計算單日金額
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static int CalcFee(DateTime start, DateTime end)
    {
        var totalMin = TimeDiff.TimeDiffMin(start, end);
        var cost = CostCalculation.TotalCost(totalMin);
        return cost;
    }

    /// <summary>
    ///     產生一日的資料
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private static IEnumerable<SingleDayFee> SingleDay(DateTime start, DateTime end, List<SingleDayFee> result)
    {
        var onlyDay = new SingleDayFee()
        {
            StartTime = start, EndTime = end,// Fee = CalcFee(start, end)
        };

        result.Add(onlyDay);

        return result;
    }

    /// <summary>
    ///     產生多日的資料
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static IEnumerable<SingleDayFee> MultiDay(DateTime start, DateTime end)
    {
        //  因為是public 所以還是要防呆
        start = DateTime(start, ref end);

        //  建立天數長度(N+1日)
        var days = Enumerable.Range(0, 1 + (end.Date-start.Date).Days );


        //  生資料
        var enumerable = days.ToList();
        var dateRange = enumerable.Select(
            offset =>
            {
                //  定義每日23:59
                var dayEnd = start.Date.AddDays(offset + 1).AddMinutes(-1);

                if (offset == 0)
                {
                    //  起始時間  & 當日23:59
                    return new SingleDayFee(start, dayEnd);
                }

                return (offset == enumerable.Count - 1)
                    //  最後一天
                    ? new SingleDayFee(end.Date, end)
                    //  中間時段
                    : new SingleDayFee(start.Date.AddDays(offset), dayEnd);
            }).ToList();

        //  算金額
        foreach (var day in dateRange)
        {
            day.Fee = CalcFee(day.StartTime, day.EndTime);
        }

        return dateRange;
    }
}