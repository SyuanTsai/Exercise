﻿namespace logicLibrary.AllenKuo.ParkingLot.UnitFunction;

/// <summary>
///     題目內容
///     假設計算停車時間的邏輯是不理會秒數，例如 13:10:59 視為是 13:10:00。
///     所以停車時間如果是13:10:59 - 13:11:10。
///     雖然他只停11秒而已，仍視為停了1分鐘
/// 
///     https://www.evernote.com/shard/s530/client/snv?noteGuid=9e16269c-6476-0e8e-1d0f-6ef712456280&noteKey=8051edac93ec4abe6d78976e209cdc49&sn=https%3A%2F%2Fwww.evernote.com%2Fshard%2Fs530%2Fsh%2F9e16269c-6476-0e8e-1d0f-6ef712456280%2F8051edac93ec4abe6d78976e209cdc49&title=20220506.Q1%2B%25E8%25A8%2588%25E7%25AE%2597%25E5%2581%259C%25E8%25BB%258A%25E5%2588%2586%25E9%2590%2598%25E6%2595%25B8
/// </summary>
public static class TimeDiff
{
    public static int TimeDiffMin(DateTime start, DateTime end)
    {
        //  低於一天，只需要處理分鐘
        if ((end.Date - start.Date).Days < 1)
        {
            var sMin = (int) start.TimeOfDay.TotalMinutes;
            var eMin = (int) end.TimeOfDay.TotalMinutes;
            var inDay = sMin - eMin;

            return Math.Abs(inDay);
        }

        //  算出差異
        var diff = (start - end).TotalMinutes;
        //  絕對值
        var abs = Math.Abs(diff);
        //  無條件進位
        var ceiling = (int) Math.Ceiling(abs);

        return ceiling;
    }
    
    /// <summary>
    ///     參考別人的寫法
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static int TimeDiffMinFor(DateTime start, DateTime end)
    {
        start = start.AddSeconds(-start.Second);
        end   = end.AddSeconds(-end.Second);

        var dt     = end - start;
        var      result = (int)dt.TotalMinutes;

        return result;
    }
}